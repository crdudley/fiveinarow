rm -rf CoverageOutput &&
    mkdir CoverageOutput &&
    cd CoverageOutput &&
    cmake -DRUN_CODE_COVERAGE=ON .. &&
    make -j8 &&
    ctest -D ExperimentalStart &&
    ctest -D ExperimentalConfigure &&
    ctest -D ExperimentalTest &&
    ctest -D ExperimentalCoverage
