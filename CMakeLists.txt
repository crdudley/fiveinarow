cmake_minimum_required( VERSION 2.8 )
project( FiveInARow )

option( RUN_CODE_COVERAGE "Run Code Coverage analysis of unit tests" OFF )
option( GENERATE_PACKAGE "Generate Distribution package" OFF )
option( BUILD_DEBUG_VERSION "Build debuggable version of game" OFF )

include( CTest )
add_definitions( -g -O2 -Wall -Wconversion -Wextra -std=c++11 )

if ( RUN_CODE_COVERAGE )
  if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
    add_definitions( -fprofile-arcs -ftest-coverage )
  else()
    message ( ERROR "Code Test Coverage currently only supported with GCC." )
  endif()
endif()

add_subdirectory(src)

find_library( CPPUNIT_LIBRARY cppunit HINTS ENV PATH )

if ( CPPUNIT_LIBRARY )
  message( STATUS "Found unit testing package" )
  add_subdirectory(test)
elseif()
  message( WARNING "CppUnit testing library not found. Tests will be disabled" )
endif()

if ( GENERATE_PACKAGE )
  include(InstallRequiredSystemLibraries)
  include(CPack)
endif()

set(CPACK_PACKAGE_INSTALL_DIRECTORY "Games")
