#ifndef FIVE_IN_A_ROW_BOARD_H
#define FIVE_IN_A_ROW_BOARD_H
//==============================================================================
// FiveInARow (c) 2018 by Clifton Dudley (clifton.r.dudley@gmail.com)
// FiveInARow is licensed under a Creative Commons Attribution 4.0 International
// License. You should have received a copy of the license along with this work.
// If not, see <http://creativecommons.org/licenses/by/4.0/>.
//==============================================================================

#include "Location.h"

#include <algorithm>
#include <array>
#include <bitset>
#include <numeric>
#include <vector>


//! \file Board.h
//! \brief Contains various enums representing the state of play, as well as the
//!        Square and Board classes.

//! \var gDimension
//! \brief The size of the board
constexpr int gDimension  = 10;

//! \var Token
//! \brief An enum class specifying the three states of each square on the board.
enum class Token { User, Ai, Blank };

//! \var Direction
//! \brief An enum describing the four directions in which a winning set of five
//!        contiguous tokens may be oriented.
enum Direction {Horizontal, DownRight, Vertical, DownLeft};

//! \var MovesLeft
//! \brief An enum describing (per Direction) the minimum number of turns
//!        required to either win (if the next AI token is placed there) or lose
//!        (if the next User token is placed there).
enum MovesLeft
{
  OneToWin,
  OneToLose,
  TwoToWin,
  TwoToLose,
  ThreeToWin,
  ThreeToLose,
  FourToWin,
  FourToLose,
  FiveToWin,
  CannotWin
};

//! \class Square
//! \brief Contains the token (if any) placed on this square, and the value
//!        of playing here next.
class Square
{
public:
  Square()
    : mDirtyFlags( 0 ),
    mToken( Token::Blank ),
    mValue{ MovesLeft::FiveToWin, MovesLeft::FiveToWin, MovesLeft::FiveToWin, MovesLeft::FiveToWin }
  {}
  Token token() const { return mToken; }
  void setToken( Token newToken ) { mToken = newToken; }
  //! Given a specific \p direction, return the MovesLeft value for a player to
  //! win with a range in that direction by placing their token on this square.
  //! \sa MovesLeft
  MovesLeft state( Direction direction ) const { return mValue[direction]; }
  //! \returns The minimum MovesLeft value for the four directions from this
  //!          square.
  //! \sa MovesLeft
  MovesLeft bestState() const { return *std::min_element( mValue.begin(), mValue.end() ); }

  //! As a secondary decision metric, calculate a wholistic metric of this
  //! square (a lower value is more likely to be played soon).
  int accumulatedState() const { return std::accumulate( mValue.begin(), mValue.end(), 0 ); }
  //! Caches the \p state for the \p direction of this square.
  void setState( Direction direction, MovesLeft state ) { mValue[direction] = state; }

  //! \var mDirtyFlags
  //! \brief Determines if the cached MovesLeft values for each direction are out of date.
  std::bitset<4> mDirtyFlags;
  
private:
  Token mToken;
  std::array< MovesLeft, 4 > mValue;
};

//! \class Board
//! \brief A Board class holding a collection of Squares.
//!
//! Note that though this class holds the Squares in a 1-dimensional vector
//! for efficient access, it presents itself to callers as a 2-dimensional
//! board by requiring the helper Location object to convert from 0-based
//! (row,col) coordinates to the storage index.
class Board
{
public:
  Board();
  static int dimension() { return gDimension; }
  //! A non-const index operator for the Square at (row,column) of this Board.
  Square& operator[]( const Location& idx );
  //! A const index operator for the Square at (row,column) of this Board
  const Square& operator[]( const Location& idx ) const;
private:
  std::vector< Square > mSquares;
};

#endif // header include guard
