#ifndef FIVE_IN_A_ROW_LOCATION
#define FIVE_IN_A_ROW_LOCATION
//==============================================================================
// FiveInARow (c) 2018 by Clifton Dudley (clifton.r.dudley@gmail.com)
// FiveInARow is licensed under a Creative Commons Attribution 4.0 International
// License. You should have received a copy of the license along with this work.
// If not, see <http://creativecommons.org/licenses/by/4.0/>.
//==============================================================================

#include <utility>
//! \file Location.h
//! \brief Contains the Location class

//! \class Location
//! \brief Simple class that defines a (row/col) location on a 2D board.
class Location
{
public:
  Location( const int row, const int col )
    : mRow( row ), mCol( col )
  {}

  Location(const Location& other)
    : mRow( other.row() ), mCol( other.col() )
  {}

  int row() const { return mRow; }
  int col() const { return mCol; }
  
  bool operator==( const Location& other )
  {
    return ( mRow == other.row() && mCol == other.col() );
  }
  bool operator!=( const Location& other )
  {
    return !( *this == other );
  }
  Location& operator=( const Location& other )
  {
    mRow = other.row();
    mCol = other.col();
    return *this;
  }

private:
  int mRow;
  int mCol;
};

#endif // header include guard
