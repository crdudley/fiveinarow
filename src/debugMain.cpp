#include "Board.h"
#include "Display.h"
#include "StateCalculator.h"

#include <cstdlib>
#include <ctime>
#include <iostream>

int main()
{
  Board gameBoard;

  bool ended = false;
  srand( static_cast< unsigned int >( time( nullptr ) ) );

  while ( !ended )
  {
    int rowPlay, colPlay;
    display::display( gameBoard );
    display::debugDisplay( gameBoard );
    std::cout << "Pick your spot `row col`: ";
    std::cin >> rowPlay >> colPlay;
    bool won = calculator::play( Location( rowPlay-1, colPlay-1 ), Token::User, gameBoard );
    if ( won )
    {
      std::cout << "Congratulations! You won!" << std::endl;
      ended = true;
    }
    else
    {
      display::debugDisplay( gameBoard );
      std::cout << "Pick comp spot `row col`: ";
      std::cin >> rowPlay >> colPlay;
      bool lost = calculator::play( Location( rowPlay-1, colPlay-1 ), Token::Ai, gameBoard );
      
      if ( lost )
      {
        std::cout << "Better luck next time" << std::endl;
        ended = true;
      }
    }
  }
  
  return 0;
}
