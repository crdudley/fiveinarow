#ifndef FIVE_IN_A_ROW_DISPLAY
#define FIVE_IN_A_ROW_DISPLAY
//==============================================================================
// FiveInARow (c) 2018 by Clifton Dudley (clifton.r.dudley@gmail.com)
// FiveInARow is licensed under a Creative Commons Attribution 4.0 International
// License. You should have received a copy of the license along with this work.
// If not, see <http://creativecommons.org/licenses/by/4.0/>.
//==============================================================================

#include <sstream>

//! \file Display.h
//! \brief Contains methods for displaying the state of the game board to stdout.
class Board;

//! \namespace <display>
//! \brief Contains the console display functions
namespace display
{
void display( const Board& board );

//! \brief for testing purposes
void display( const Board& board, std::stringstream& buffer );
//! \brief for testing purposes
void debugDisplay( const Board& board );
}
#endif // header include guard
