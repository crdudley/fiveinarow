#include "Board.h"
#include "Location.h"

Board::Board()
{
  mSquares.resize( gDimension*gDimension, Square() );
}

Square& Board::operator[]( const Location& idx)
{
  return mSquares[ idx.row() * gDimension + idx.col() ];
}

const Square& Board::operator[]( const Location& idx) const
{
  return mSquares[ idx.row() * gDimension + idx.col() ];
}

