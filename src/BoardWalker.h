#ifndef FIVE_IN_A_ROW_BOARD_WALKER
#define FIVE_IN_A_ROW_BOARD_WALKER
//==============================================================================
// FiveInARow (c) 2018 by Clifton Dudley (clifton.r.dudley@gmail.com)
// FiveInARow is licensed under a Creative Commons Attribution 4.0 International
// License. You should have received a copy of the license along with this work.
// If not, see <http://creativecommons.org/licenses/by/4.0/>.
//==============================================================================

#include "Board.h"

#include <stdexcept>
#include <utility>
#include <vector>

//! \file BoardWalker.h
//! \brief This file contains the boardRange function, which generates linearly
//!        contiguous collections of Locations utilizing the various BoardWalker
//!        helper classes.
//!
//! The BoardWalker helper classes simplify the process of traversing a board in
//! various directions when attempting to determine the local effect of playing
//! a token. They are used indirectly through the templated boardRange function,
//! which returns all Locations in the templated direction that could be affected
//! by placing a token at the provided location.

//! \namespace <walk>
//! \brief Contains the boardRange function and BoardWalker helper classes
namespace walk
{

//! \brief Templated over the four directions, this free function generates a
//!        range of Locations whose state would be affected by placing a token
//!        at \p origin.
//! \throws std::runtime_error if \p origin is outside the boundaries of the Board.
template< typename T >
std::vector< Location > boardRange( const Location& origin )
{
  std::vector< Location > locations;
  Location start = T::begin( origin );
  Location finish = T::end( origin );
  for ( Location iter = std::move(start); iter != finish; iter = std::move(T::next( iter ) ) )
  {
    locations.push_back( iter );
  }
  return locations;
}

//! \class HorizontalWalker
//! \brief An iterator-like class that helps boardRange generate horizontal
//!        collections of Locations
class HorizontalWalker
{
public:
  //! Given a \p origin location, returns the location 4 spaces in front of
  //! \p origin horizontally, or the beginning of the board, whichever is
  //! closer
  static Location begin( const Location& origin );
  //! Given a \p origin location, returns the location 4 spaces after \p origin
  //! horizontally, or the end of the board, whichever is closer
  static Location end( const Location& origin );
  static Location next( const Location& origin );
private:
  HorizontalWalker();
};

//! \class VerticalWalker
//! \brief An iterator-like class that helps boardRange generate vertical
//!        collections of Locations
class VerticalWalker
{
public:
  //! Given a \p origin location, returns the location 4 spaces in front of
  //! \p origin vertically, or the beginning of the board, whichever is
  //! closer
  static Location begin( const Location& origin );
  //! Given a \p origin location, returns the location 4 spaces after \p origin
  //! vertically, or the end of the board, whichever is closer
  static Location end( const Location& origin );
  static Location next( const Location& origin );
private:
  VerticalWalker();
};

//! \class DownLeftWalker
//! \brief An iterator-like class that helps boardRange generate collections
//!        Locations that trend down and to the left.
class DownLeftWalker
{
public:
  //! Given a \p origin location, returns the location 4 spaces above and to the
  //! right of \p origin, or the edge of the board, whichever is closer
  static Location begin( const Location& origin );
  //! Given a \p origin location, returns the location 4 spaces below and to the
  //! right of \p origin, or the edge of the board, whichever is closer
  static Location end( const Location& origin );
  static Location next( const Location& origin );
private:
  DownLeftWalker();
};

//! \class DownRightWalker
//! \brief An iterator-like class that helps boardRange generate collections
//!        Locations that trend down and to the right.
class DownRightWalker
{
public:
  //! Given a \p origin location, returns the location 4 spaces above and to the
  //! left of \p origin, or the edge of the board, whichever is closer
  static Location begin( const Location& origin );
  //! Given a \p origin location, returns the location 4 spaces below and to the
  //! right of \p origin, or the edge of the board, whichever is closer
  static Location end( const Location& origin );
  static Location next( const Location& origin );
private:
  DownRightWalker();
};

/// @private
inline void checkOrigin( const Location& origin )
{
  if ( origin.row() < 0
    || origin.col() < 0
    || origin.row() >= gDimension
    || origin.col() >= gDimension )
  {
    throw std::runtime_error("Bad Location for Walker origin\n");
  }
}

} // namespace walk

#endif // header include guard
