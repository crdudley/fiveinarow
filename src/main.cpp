#include "Board.h"
#include "Display.h"
#include "StateCalculator.h"

#include <iostream>

int main()
{
  Board gameBoard;

  bool ended = false;

  // Until the game is finished:
  //  Prompt for a (valid) move from the user.
  //  Calculate the result of the users' play
  //    if they win, congratulate and exit
  //  Determine the best place for the AI to play.
  //    if AI won, better luck and exit
  while ( !ended )
  {
    int rowPlay, colPlay;
    display::display( gameBoard );
    std::cout << "Pick your spot, row number followed by column number: ";
    std::cin >> rowPlay >> colPlay;
    // User input is 1-based index, but the board is a 0-based index, so we convert
    //  the values before checking the board.
    rowPlay--; colPlay--;
    while ( gameBoard[Location( rowPlay, colPlay )].token() != Token::Blank ) 
    {
      std::cout << "That spot is already taken. Try again: ";
      std::cin >> rowPlay >> colPlay;
      // Convert to 0-based index
      rowPlay--; colPlay--;
    }
    bool won = calculator::play( Location( rowPlay, colPlay ), Token::User, gameBoard );
    if ( won )
    {
      display::display( gameBoard );
      std::cout << "Congratulations! You won!" << std::endl;
      ended = true;
    }
    else
    {
      if ( calculator::computerChoose( gameBoard ) )
      {
        display::display( gameBoard );
        std::cout << "Better luck next time" << std::endl;
        ended = true;
      }
    }
  }
  
  return 0;
}
