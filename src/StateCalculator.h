#ifndef FIVE_IN_A_ROW_BOARD_UPDATER
#define FIVE_IN_A_ROW_BOARD_UPDATER
//==============================================================================
// FiveInARow (c) 2018 by Clifton Dudley (clifton.r.dudley@gmail.com)
// FiveInARow is licensed under a Creative Commons Attribution 4.0 International
// License. You should have received a copy of the license along with this work.
// If not, see <http://creativecommons.org/licenses/by/4.0/>.
//==============================================================================

#include "Board.h"
#include "BoardWalker.h"

//! \file StateCalculator.h
//! \brief Contains various AI related functions

//! \namespace <calculator>
//! \brief Includes a collection of free functions that process the moves on
//!   the board and determine the locations on the board most likely to achieve
//!   a quick AI victory.
namespace calculator
{
//! \brief Places \p token on \p board at \p location, determines if either
//! party has won, sets the correct dirty flags for each of the nearby squares,
//! and calls updateBoard.
//! \returns true if placing the token causes someone to win, false otherwise
bool play( const Location& location, const Token token, Board& board );

//! \brief Given a linearly contiguous collection of Locations on \p board,
//! calculate the minimum number of tokens that will cause either party to win.
//!
//! For example if the vector consists of { _, _, X, X, X } where _ is a blank
//! space and X represents the User, then the result of calculateState would be
//! TwoToLose, since all that is required for the AI to lose is for the User to
//! place two more tokens in this range. However, if the vector consisted of
//! { _, O, O, X, _, _ X, O, _}, then the result would be CANNOTWIN, since
//! neither player would be able to get five pieces in a row in this range.
MovesLeft calculateState( const std::vector< Location >& range, const Board& board );

//! \brief Traverses the \p board, and updates every Square that has been
//!        dirtied, by generating a contiguous range in the correct direction
//!        and calling calculateState.
//! \sa Square::mDirtyFlags
void updateBoard( Board& board );

//! \brief Traverses the \p board looking for the minimum MovesLeft value for
//!        any direction on the board. Calls play on that location with an AI
//!        token.
//!
//! The philosophy here is that if the User is close to winning at a certain
//! location (say they have 4 in a row next to it), the AI wants to place there
//! unless there is another location where the AI has 4 out of 5 spaces as well.
//! If there are multiple Squares with the same bestState, then a secondary
//! metric is calculated. If more than one of the best Squares has the same
//! secondary metric, then one of them is chosen at random.
//! \sa Square::bestState
//! \sa Square::accumulatedState
//! \returns True if the AI has won by placing their token, false otherwise.
bool computerChoose( Board& board );
}
#endif // header include guard
