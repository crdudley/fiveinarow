#include "BoardWalker.h"

#include <algorithm>
#include <utility>

namespace walk
{

constexpr int kMinIndex = 0;

/// The distance between the beginning and end of 5 contiguous pieces
constexpr int kSpan = 4;

/// The end: "one past" the last element in the range.
constexpr int kOnePast = 5;

Location HorizontalWalker::begin( const Location& origin )
{
  checkOrigin( origin );
  return Location( origin.row(), std::max( kMinIndex, origin.col() - kSpan ) );
}

Location VerticalWalker::begin( const Location& origin )
{
  checkOrigin( origin );
  return Location( std::max( kMinIndex, origin.row() - kSpan ), origin.col() );
}

Location DownLeftWalker::begin( const Location& origin )
{
  checkOrigin( origin );
  int upperLimit = std::max( origin.row() - kSpan, kMinIndex );
  int rightLimit = std::min( origin.col() + kSpan, gDimension - 1 );
  int closerDistance = std::min( origin.row() - upperLimit, rightLimit - origin.col() );
  return Location(
    origin.row() - closerDistance,
    origin.col() + closerDistance );
}

Location DownRightWalker::begin( const Location& origin )
{
  checkOrigin( origin );
  int upperLimit = std::max( origin.row() - kSpan, kMinIndex );
  int leftLimit = std::max( origin.col() - kSpan, kMinIndex );
  int closerDistance = std::min( origin.row() - upperLimit, origin.col() - leftLimit );
  return Location(
    origin.row() - closerDistance,
    origin.col() - closerDistance );
}

Location HorizontalWalker::end( const Location& origin )
{
  checkOrigin( origin );
  return Location( origin.row(), std::min( gDimension, origin.col() + kOnePast ) );
}

Location VerticalWalker::end( const Location& origin )
{
  checkOrigin( origin );
  return Location( std::min( gDimension, origin.row() + kOnePast ), origin.col() );
}

Location DownLeftWalker::end( const Location& origin )
{
  checkOrigin( origin );
  int lowerLimit = std::min( origin.row() + kOnePast, gDimension );
  int leftLimit = std::max( origin.col() - kOnePast, -1 );
  int closerDistance = std::min( lowerLimit - origin.row(), origin.col() - leftLimit );
  return Location( origin.row() + closerDistance, origin.col() - closerDistance );
}

Location DownRightWalker::end( const Location& origin )
{
  checkOrigin( origin );
  int lowerLimit = std::min( origin.row() + kOnePast, gDimension );
  int rightLimit = std::min( origin.col() + kOnePast, gDimension );
  int closerDistance = std::min( lowerLimit - origin.row(), rightLimit - origin.col() );
  return Location( origin.row() + closerDistance, origin.col() + closerDistance );
}

Location HorizontalWalker::next( const Location& current )
{
  return Location( current.row(), current.col() + 1 );
}

Location VerticalWalker::next( const Location& current )
{
  return Location( current.row() + 1, current.col() );
}

Location DownLeftWalker::next( const Location& current )
{
  return Location( current.row() + 1, current.col() - 1 );
}

Location DownRightWalker::next( const Location& current )
{
  return Location( current.row() + 1, current.col() + 1 );
}

} // namespace walk
