#include "Display.h"
#include "Board.h"

#include <sstream>
#include <iostream>
#include <iomanip>

namespace display
{

void display( const Board& board, std::stringstream& buffer )
{
  std::streambuf* originalBuffer = std::cout.rdbuf( buffer.rdbuf() );
  display( board );
  std::cout.rdbuf( originalBuffer );
}

void display( const Board& board )
{
  // numbers across top
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int tens = i/10;
    if ( !tens )
    {
      std::cout << "  ";
    }
    else
    {
      std::cout << " " << tens;
    }
  }
  std::cout << std::endl;
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int ones = i%10;
    std::cout << ones << " ";
  }
  std::cout << std::endl;
  
  //top border
  std::cout << "   " << std::setw( 2 + 2*gDimension ) << std::setfill('#');
  std::cout << '#' << std::endl;
  std::cout << std::setfill(' ');
  for ( int i = 0; i < gDimension; i++ )
  {
    std::cout << std::setw(3) << i+1;
    std::cout << '#';
    for ( int j = 0; j < gDimension; j++ )
    {
      Token inSquare = board[Location(i,j)].token();
      switch (inSquare)
      {
      case Token::Blank:
        std::cout << " |";
        break;
      case Token::User:
        std::cout << "X|";
        break;
      case Token::Ai:
        std::cout << "O|";
        break;
      }
    }
    std::cout << '#';
    std::cout << std::setw(3) << i+1;
    std::cout << std::endl;
  }
  //bottom border
  std::cout << "   " << std::setw( 2 + 2*gDimension ) << std::setfill('#');
  std::cout << '#' << std::endl;
  std::cout << std::setfill(' ');

  // numbers across bottom
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int tens = i/10;
    if ( !tens )
    {
      std::cout << "  ";
    }
    else
    {
      std::cout << tens << " " ;
    }
  }
  std::cout << std::endl;
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int ones = i%10;
    std::cout << ones << " ";
  }

  std::cout << std::endl;
  std::cout << std::endl;
}

void debugDisplay( const Board& board )
{
  // numbers across top
  std::cout << "    ";
  for ( int i = 1; i <= 2*gDimension; i++ )
  {
    int tens = i/20;
    if ( !tens )
    {
      std::cout << "  ";
    }
    else
    {
      std::cout << " " << tens;
    }
  }
  std::cout << std::endl;
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int ones = i%10;
    std::cout << " " << ones;
  }
  std::cout << std::endl;
  
  //top border
  std::cout << "   " << std::setw( 2 + 2*gDimension ) << std::setfill('#');
  std::cout << '#' << std::endl;
  std::cout << std::setfill(' ');
  for ( int i = 0; i < gDimension; i++ )
  {
    std::cout << std::setw(3) << i+1;
    std::cout << '#';
    for ( int j = 0; j < gDimension; j++ )
    {
      MovesLeft inSquare = board[Location(i,j)].bestState();
      std::cout << std::setw(2) << inSquare;
    }
    std::cout << '#';
    std::cout << std::setw(3) << i+1;
    std::cout << std::endl;
  }
  //bottom border
  std::cout << "   " << std::setw( 2 + 2*gDimension ) << std::setfill('#');
  std::cout << '#' << std::endl;
  std::cout << std::setfill(' ');

  // numbers across bottom
  std::cout << "    ";
  for ( int i = 1; i <= 2*gDimension; i++ )
  {
    int tens = i/20;
    if ( !tens )
    {
      std::cout << "  ";
    }
    else
    {
      std::cout << " " << tens;
    }
  }
  std::cout << std::endl;
  std::cout << "    ";
  for ( int i = 1; i <= gDimension; i++ )
  {
    int ones = i%10;
    std::cout << " " << ones;
  }

  std::cout << std::endl;
  std::cout << std::endl;
}

}
