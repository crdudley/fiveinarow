#include "StateCalculator.h"

#include "Board.h"
#include "BoardWalker.h"

#include <algorithm>
#include <iostream>
#include <random>

namespace calculator
{

bool play( const Location& location, const Token token, Board& board )
{
  Square& played = board[location];
  played.setToken( token );
  if ( ( played.bestState() == MovesLeft::OneToWin && token == Token::Ai )
   || ( played.bestState() == MovesLeft::OneToLose && token == Token::User ) )
  {
    return true;
  }
  played.setState( Direction::Horizontal, MovesLeft::CannotWin );
  played.setState( Direction::DownRight, MovesLeft::CannotWin );
  played.setState( Direction::Vertical, MovesLeft::CannotWin );
  played.setState( Direction::DownLeft, MovesLeft::CannotWin );
  //Get range in each direction, and mark all Squares dirty in that direction
  auto horizontalRange = walk::boardRange<walk::HorizontalWalker>( location );
  for ( auto& nearby : horizontalRange )
  {
    board[nearby].mDirtyFlags.set(Direction::Horizontal);
  }

  auto downRightRange = walk::boardRange<walk::DownRightWalker>( location );
  for ( auto& nearby : downRightRange )
  {
    board[nearby].mDirtyFlags.set(Direction::DownRight);
  }

  auto verticalRange = walk::boardRange<walk::VerticalWalker>( location );
  for ( auto& nearby : verticalRange )
  {
    board[nearby].mDirtyFlags.set(Direction::Vertical);
  }

  auto downLeftRange = walk::boardRange<walk::DownLeftWalker>( location );
  for ( auto& nearby : downLeftRange )
  {
    board[nearby].mDirtyFlags.set(Direction::DownLeft);
  }

  updateBoard( board );
  return false;
}

void updateBoard( Board& board )
{
  for ( int i = 0; i < board.dimension(); i++ )
  {
    for ( int j = 0; j < board.dimension(); j++ )
    {
      Location currentLocation = Location( i, j );
      Square& currentSquare = board[currentLocation];
      if ( currentSquare.token() != Token::Blank
        || currentSquare.mDirtyFlags.none() )
      {
        continue;
      }

      if ( currentSquare.mDirtyFlags.test( Direction::Horizontal ) )
      {
        MovesLeft updatedState = calculateState(walk::boardRange<walk::HorizontalWalker>( currentLocation ), board );
        currentSquare.mDirtyFlags.reset( Direction::Horizontal );
        currentSquare.setState( Direction::Horizontal, updatedState );
      }

      if ( currentSquare.mDirtyFlags.test( Direction::DownRight ) )
      {
        MovesLeft updatedState = calculateState(walk::boardRange<walk::DownRightWalker>( currentLocation ), board );
        currentSquare.mDirtyFlags.reset( Direction::DownRight );
        currentSquare.setState( Direction::DownRight, updatedState );
      }

      if ( currentSquare.mDirtyFlags.test( Direction::Vertical ) )
      {
        MovesLeft updatedState = calculateState(walk::boardRange<walk::VerticalWalker>( currentLocation ), board );
        currentSquare.mDirtyFlags.reset( Direction::Vertical );
        currentSquare.setState( Direction::Vertical, updatedState );
      }

      if ( currentSquare.mDirtyFlags.test( Direction::DownLeft ) )
      {
        MovesLeft updatedState = calculateState(walk::boardRange<walk::DownLeftWalker>( currentLocation ), board );
        currentSquare.mDirtyFlags.reset( Direction::DownLeft );
        currentSquare.setState( Direction::DownLeft, updatedState );
      }
    }
  }
}

MovesLeft calculateState( const std::vector< Location >& range, const Board& board )
{
  if ( range.size() < 5 )
  {
    return MovesLeft::CannotWin;
  }

  std::vector< Token > rangeTokens( range.size() );
  std::transform(
    range.begin(),
    range.end(),
    rangeTokens.begin(),
    [&board](Location loc){ return board[loc].token();} );
  
  MovesLeft bestState = MovesLeft::CannotWin;
  std::vector< Token >::iterator firstOfFive, lastOfFive;
  for ( firstOfFive = rangeTokens.begin(), lastOfFive = rangeTokens.begin()+4;
        lastOfFive != rangeTokens.end();
        firstOfFive++, lastOfFive++ )
  {
    int numberUser = static_cast< int >( std::count( firstOfFive,lastOfFive+1, Token::User ) );
    int numberAi = static_cast< int >( std::count( firstOfFive, lastOfFive+1, Token::Ai ) );
    MovesLeft localState = MovesLeft::FiveToWin;
    if ( numberUser && numberAi )
    {
      localState = MovesLeft::CannotWin;
    }
    else if ( numberUser )
    {
      switch( numberUser )
      {
      case 1: localState = MovesLeft::FourToLose; break;
      case 2: localState = MovesLeft::ThreeToLose; break;
      case 3: localState = MovesLeft::TwoToLose; break;
      case 4: localState = MovesLeft::OneToLose; break;
      default: break;
      }
    }
    else if ( numberAi )
    {
      switch( numberAi )
      {
      case 1: localState = MovesLeft::FourToWin; break;
      case 2: localState = MovesLeft::ThreeToWin; break;
      case 3: localState = MovesLeft::TwoToWin; break;
      case 4: localState = MovesLeft::OneToWin; break;
      default: break;
      }
    }

    if ( localState < bestState )
    {
      bestState = localState;
    }
  }
  return bestState;
}

bool computerChoose ( Board& board )
{
  MovesLeft bestState = MovesLeft::FiveToWin;
  std::vector< Location > bestPlays;
  for ( int i = 0; i < board.dimension(); i++ )
  {
    for ( int j = 0; j < board.dimension(); j++ )
    {
      if ( board[Location(i,j)].bestState() < bestState )
      {
        bestPlays.clear();
        bestState = board[Location(i,j)].bestState();
      }

      if ( board[Location(i,j)].bestState() == bestState )
      {
        bestPlays.push_back(Location(i,j));
      }
    }
  }

  std::vector< Location > refinedBest;
  int bestSummedState = 4 * MovesLeft::FiveToWin;
  for ( auto loc : bestPlays )
  {
    if ( board[loc].accumulatedState() < bestSummedState )
    {
      refinedBest.clear();
      bestSummedState = board[loc].accumulatedState();
    }

    if ( board[loc].accumulatedState() == bestSummedState )
    {
      refinedBest.push_back(loc);
    }
  }

  // Static initialize our random number generator
  static std::random_device randomDevice;
  static std::mt19937 generator( randomDevice() );
  int locIndex = static_cast< int >( generator() % refinedBest.size() );
  std::cout << "Computer chose " << refinedBest[locIndex].row()+1 << ", " << refinedBest[locIndex].col()+1 << std::endl;
  return play( refinedBest[locIndex], Token::Ai, board );
}
  
}
