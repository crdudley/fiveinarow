#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class SquareState : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( SquareState );
  CPPUNIT_TEST( RoundTripState );
  CPPUNIT_TEST( BestState );
  CPPUNIT_TEST( AccumulatedState );
  CPPUNIT_TEST_SUITE_END();

public:
  SquareState()
    : CppUnit::TestCase( "Round Trip State" )
    {}
  void RoundTripState()
  {
    Square testSquare;
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.state( Direction::Horizontal ) );
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.state( Direction::Vertical ) );
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.state( Direction::DownLeft ) );
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.state( Direction::DownRight ) );

    /// Test set horizontal
    testSquare.setState( Direction::Horizontal, MovesLeft::FourToLose );
    CPPUNIT_ASSERT( MovesLeft::FourToLose == testSquare.state( Direction::Horizontal ) );
    /// Return horizontal to original value and test again
    testSquare.setState( Direction::Horizontal, MovesLeft::FiveToWin );
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.state( Direction::Horizontal ) );

    /// Test vertical
    testSquare.setState( Direction::Vertical, MovesLeft::TwoToWin );
    CPPUNIT_ASSERT( MovesLeft::TwoToWin == testSquare.state( Direction::Vertical ) );

    /// Test Down Left
    testSquare.setState( Direction::DownLeft, MovesLeft::OneToLose );
    CPPUNIT_ASSERT( MovesLeft::OneToLose == testSquare.state( Direction::DownLeft ) );
    /// Vertical should still be the same
    CPPUNIT_ASSERT( MovesLeft::TwoToWin == testSquare.state( Direction::Vertical ) );

    testSquare.setState( Direction::DownRight, MovesLeft::CannotWin );
    CPPUNIT_ASSERT( MovesLeft::CannotWin == testSquare.state( Direction::DownRight ) );

    /// MovesLeft order consistent
    CPPUNIT_ASSERT( MovesLeft::CannotWin > MovesLeft::FiveToWin );
    CPPUNIT_ASSERT( MovesLeft::FiveToWin > MovesLeft::FourToLose );
    CPPUNIT_ASSERT( MovesLeft::FourToLose > MovesLeft::FourToWin );
    CPPUNIT_ASSERT( MovesLeft::FourToWin > MovesLeft::ThreeToLose );
    CPPUNIT_ASSERT( MovesLeft::ThreeToLose > MovesLeft::ThreeToWin );
    CPPUNIT_ASSERT( MovesLeft::ThreeToWin > MovesLeft::TwoToLose );
    CPPUNIT_ASSERT( MovesLeft::TwoToLose > MovesLeft::TwoToWin );
    CPPUNIT_ASSERT( MovesLeft::TwoToWin > MovesLeft::OneToLose );
    CPPUNIT_ASSERT( MovesLeft::OneToLose > MovesLeft::OneToWin );
  }

  void BestState()
  {
    Square testSquare;
    CPPUNIT_ASSERT( MovesLeft::FiveToWin == testSquare.bestState() );

    testSquare.setState( Direction::Horizontal, MovesLeft::FourToLose );
    CPPUNIT_ASSERT( MovesLeft::FourToLose == testSquare.bestState() );

    /// Test a state in the middle of the direction array
    /// Also tests multiple different states in array
    testSquare.setState( Direction::DownRight, MovesLeft::ThreeToWin );
    CPPUNIT_ASSERT( MovesLeft::ThreeToWin == testSquare.bestState() );

    /// If best is taken away, revert correctly to previous best?
    testSquare.setState( Direction::DownRight, MovesLeft::CannotWin );
    CPPUNIT_ASSERT( MovesLeft::FourToLose == testSquare.bestState() );

    testSquare.setState( Direction::Vertical, MovesLeft::OneToWin );
    testSquare.setState( Direction::DownLeft, MovesLeft::OneToWin );
    CPPUNIT_ASSERT( MovesLeft::OneToWin == testSquare.bestState() );
    testSquare.setState( Direction::Vertical, MovesLeft::CannotWin );
    CPPUNIT_ASSERT( MovesLeft::OneToWin == testSquare.bestState() );
  }

  void AccumulatedState()
  {
    Square testSquare;
    CPPUNIT_ASSERT( 4 * MovesLeft::FiveToWin == testSquare.accumulatedState() );

    testSquare.setState( Direction::Vertical, MovesLeft::OneToWin );
    testSquare.setState( Direction::DownLeft, MovesLeft::OneToWin );
    CPPUNIT_ASSERT( 2 * ( MovesLeft::FiveToWin + MovesLeft::OneToWin )  == testSquare.accumulatedState() );

  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("SquareStateResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(SquareState::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
