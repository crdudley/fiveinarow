#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class WalkerEnd : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( WalkerEnd );
  CPPUNIT_TEST( End );
  CPPUNIT_TEST_SUITE_END();

public:
  WalkerEnd()
    : CppUnit::TestCase( "Board Walker End" )
    {}

  void End()
  {
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::end( Location(4,7)).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::end( Location(4,7)).col(), gDimension );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::end( Location(1,1)).row(), 1 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::end( Location(1,1)).col(), 6 );

    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::end( Location(2,3)).row(), 7 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::end( Location(2,3)).col(), 3 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::end( Location(8,0)).row(), gDimension );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::end( Location(8,0)).col(), 0 );

    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::end( Location(4,3)).row(), 8 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::end( Location(4,3)).col(), -1 );
    CPPUNIT_ASSERT_EQUAL(
      walk::DownLeftWalker::end( Location(8,8)).row(),
      std::min( 17,  gDimension ) );
    CPPUNIT_ASSERT_EQUAL(
      walk::DownLeftWalker::end( Location(8,8)).col(),
      std::max(-1, 16 - gDimension ) );

    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::end( Location(7,8)).row(), gDimension - 1 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::end( Location(7,8)).col(), gDimension );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::end( Location(9,6)).row(), gDimension );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::end( Location(9,6)).col(), gDimension - 3 );

  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("WalkerEndResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(WalkerEnd::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
