#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class WalkerNext : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( WalkerNext );
  CPPUNIT_TEST( Next );
  CPPUNIT_TEST_SUITE_END();

public:
  WalkerNext()
    : CppUnit::TestCase( "Board Walker Next" )
    {}
  void Next()
  {
    Location origin = Location(3,3);
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::next( origin ).row(), 3 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::next( origin ).col(), 4 );

    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::next( origin ).col(), 3 );

    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::next( origin ).col(), 2 );

    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::next( origin ).col(), 4 );
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("WalkerNextResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(WalkerNext::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
