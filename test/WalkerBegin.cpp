#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class WalkerBegin : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( WalkerBegin );
  CPPUNIT_TEST( Begin );
  CPPUNIT_TEST_SUITE_END();

public:
  WalkerBegin()
    : CppUnit::TestCase( "Board Walker Begin" )
    {}

  void Begin()
  {
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::begin( Location(4,3)).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::begin( Location(4,3)).col(), 0 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::begin( Location(1,8)).row(), 1 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::begin( Location(1,8)).col(), 4 );

    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::begin( Location(2,3)).row(), 0 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::begin( Location(2,3)).col(), 3 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::begin( Location(8,0)).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::begin( Location(8,0)).col(), 0 );

    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::begin( Location(3,3)).row(), 0 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::begin( Location(3,3)).col(), 6 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::begin( Location(2,9)).row(), 2 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::begin( Location(2,9)).col(), gDimension - 1 );

    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::begin( Location(7,2)).row(), 5 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::begin( Location(7,2)).col(), 0 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::begin( Location(1,6)).row(), 0 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::begin( Location(1,6)).col(), 5 );

    CPPUNIT_ASSERT_THROW(
      walk::HorizontalWalker::begin(Location(-1,2)),
      std::runtime_error);
    CPPUNIT_ASSERT_THROW(
      walk::HorizontalWalker::begin(Location(2,-1)),
      std::runtime_error);
  }
  
  void Next()
  {
    Location origin = Location(3,3);
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::next( origin ).row(), 3 );
    CPPUNIT_ASSERT_EQUAL( walk::HorizontalWalker::next( origin ).col(), 4 );

    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::VerticalWalker::next( origin ).col(), 3 );

    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::DownLeftWalker::next( origin ).col(), 2 );

    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::next( origin ).row(), 4 );
    CPPUNIT_ASSERT_EQUAL( walk::DownRightWalker::next( origin ).col(), 4 );
}

  void Range()
  {
    Location topLeftOrigin = Location(1,2);
    int rangeSize = static_cast< int >(
      walk::boardRange< walk::HorizontalWalker >( topLeftOrigin ).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 7 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 4 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );

    Location topRightOrigin = Location(1,gDimension-2);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 3 );

    Location bottomLeftOrigin = Location(gDimension-1,2);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 7 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 5 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 5 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 3 );

    Location bottomRightOrigin = Location(gDimension-4,gDimension-5);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 9 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
  }

};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("WalkerBeginResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(WalkerBegin::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
