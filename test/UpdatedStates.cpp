#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"
#include "StateCalculator.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class UpdatedStates : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( UpdatedStates );
  CPPUNIT_TEST( UpdatedBoard );
  CPPUNIT_TEST( Noncontiguous );
  CPPUNIT_TEST( CannotWin );
  CPPUNIT_TEST_SUITE_END();

public:
  UpdatedStates()
    : CppUnit::TestCase( "Board Updates Correctly" )
    {}

  void UpdatedBoard()
  {
    Board testBoard;
    Location testLocation(5,5);
    calculator::play( testLocation, Token::User, testBoard );
    std::vector<Location> horizontalNeighbors = walk::boardRange< walk::HorizontalWalker >( testLocation );
    std::vector<Location> verticalNeighbors = walk::boardRange< walk::VerticalWalker >( testLocation );
    std::vector<Location> downRightNeighbors = walk::boardRange< walk::DownRightWalker >( testLocation );
    std::vector<Location> downLeftNeighbors = walk::boardRange< walk::DownLeftWalker >( testLocation );
    horizontalNeighbors.erase( horizontalNeighbors.begin() + 4 );
    verticalNeighbors.erase( verticalNeighbors.begin() + 4 );
    downRightNeighbors.erase( downRightNeighbors.begin() + 4 );
    downLeftNeighbors.erase( downLeftNeighbors.begin() + 4 );

    for ( auto loc : horizontalNeighbors ) {
      CPPUNIT_ASSERT( testBoard[loc].state( Direction::Horizontal ) == MovesLeft::FourToLose );
    }

    for ( auto loc : verticalNeighbors ) {
      CPPUNIT_ASSERT( testBoard[loc].state( Direction::Vertical ) == MovesLeft::FourToLose );
    }

    for ( auto loc : downRightNeighbors ) {
      CPPUNIT_ASSERT( testBoard[loc].state( Direction::DownRight ) == MovesLeft::FourToLose );
    }

    for ( auto loc : downLeftNeighbors ) {
      CPPUNIT_ASSERT( testBoard[loc].state( Direction::DownLeft ) == MovesLeft::FourToLose );
    }
  }  

  void Noncontiguous()
  {
    Board board;
    Location playOne( 4 , 3 );
    Location playTwo( 8 , 7 );
    Location inBetween( 6, 5 );
    calculator::play( playOne, Token::User, board );
    CPPUNIT_ASSERT( board[playTwo].state( Direction::DownRight ) == MovesLeft::FourToLose );
    CPPUNIT_ASSERT( board[inBetween].state( Direction::DownRight ) == MovesLeft::FourToLose );
    calculator::play( playTwo, Token::User, board );
    CPPUNIT_ASSERT( board[inBetween].state( Direction::DownRight ) == MovesLeft::ThreeToLose );
  }

  void CannotWin()
  {
    Board board;
    Location compPlayOne( 7, 2 );
    Location userPlayOne( 7, 3 );
    Location testOne( 7, 1 );
    Location testTwo( 7, 4 );
    Location compPlayTwo( 7, 5 );
    Location userPlayTwo( 7, 6 );

    calculator::play( compPlayOne, Token::Ai, board );
    CPPUNIT_ASSERT( board[testOne].state( Direction::Horizontal ) == MovesLeft::FourToWin );
    CPPUNIT_ASSERT( board[testTwo].state( Direction::Horizontal ) == MovesLeft::FourToWin );

    calculator::play( userPlayOne, Token::User, board );
    CPPUNIT_ASSERT( board[testOne].state( Direction::Horizontal ) == MovesLeft::CannotWin );
    CPPUNIT_ASSERT( board[testTwo].state( Direction::Horizontal ) == MovesLeft::FourToLose );

    calculator::play( compPlayTwo, Token::Ai, board );
    calculator::play( userPlayTwo, Token::User, board );
    CPPUNIT_ASSERT( board[testOne].state( Direction::Horizontal ) == MovesLeft::CannotWin );
    CPPUNIT_ASSERT( board[testTwo].state( Direction::Horizontal ) == MovesLeft::CannotWin );
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("UpdatedStatesResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(UpdatedStates::suite());
  bool success = runner.run();

  return success ? 0 : -1;
}
