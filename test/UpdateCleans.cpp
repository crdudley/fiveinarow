#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"
#include "StateCalculator.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class UpdateCleans : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( UpdateCleans );
  CPPUNIT_TEST( CleanBoard );
  CPPUNIT_TEST_SUITE_END();

public:
  UpdateCleans()
    : CppUnit::TestCase( "Clean Board After Update" )
    {}

  void CleanBoard()
  {
    Board testBoard;
    Location testLocation(5,5);
    CPPUNIT_ASSERT( testBoard[testLocation].mDirtyFlags.none() );
    testBoard[testLocation].mDirtyFlags.flip();
    CPPUNIT_ASSERT( testBoard[testLocation].mDirtyFlags.all() );
    calculator::updateBoard( testBoard );
    CPPUNIT_ASSERT( testBoard[testLocation].mDirtyFlags.none() );
  }  

};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("UpdateCleansResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(UpdateCleans::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
