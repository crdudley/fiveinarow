#include "Board.h"
#include "BoardWalker.h"
#include "Display.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <limits>
#include <string>
#include <sstream>

class BoardDisplay : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( BoardDisplay );
  CPPUNIT_TEST( Display );
  CPPUNIT_TEST_SUITE_END();

public:
  BoardDisplay()
    : CppUnit::TestCase( "Board Displays Properly" )
    {}
  void Display()
  {
    Board board;
    Location locOne( 8, 2 );
    Location locTwo( 1, 4 );
    board[locOne].setToken( Token::User );
    board[locTwo].setToken( Token::Ai );
    std::stringstream output;
    display::display( board, output );

    std::string line;
    //ignore first four lines
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    // next line should be row 1.
    std::getline( output, line );
    std::size_t leadingEdge = line.find('#');
    std::size_t compSpot = line.find('O');
    CPPUNIT_ASSERT( compSpot != std::string::npos );
    CPPUNIT_ASSERT( compSpot - leadingEdge == 9 );
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    std::getline( output, line );
    // next line should be row 8.
    std::getline( output, line );
    std::size_t userSpot = line.find('X');
    CPPUNIT_ASSERT( userSpot != std::string::npos );
    CPPUNIT_ASSERT( userSpot - leadingEdge == 5 );    
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("BoardDisplayResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(BoardDisplay::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
