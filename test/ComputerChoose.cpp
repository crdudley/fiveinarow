#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"
#include "StateCalculator.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <fstream>
#include <string>


class ComputerChoose : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( ComputerChoose );
  CPPUNIT_TEST( CorrectChoice );
  CPPUNIT_TEST_SUITE_END();

public:
  ComputerChoose()
    : CppUnit::TestCase( "Computer Chooses Best Location" )
    {}

  void CorrectChoice()
  {
    // Need to call srand here because we're not calling main, so
    // otherwise the randomizer wouldn't get initialized
    srand( static_cast< unsigned int >( time( nullptr ) ) );
    Board board;
    Location userPlayOne(4,5);
    Location userPlayTwo(0,5);
    Location userPlayThree(3,3);
    calculator::play( userPlayOne, Token::User, board );
    calculator::play( userPlayTwo, Token::User, board );
    calculator::computerChoose( board );
    CPPUNIT_ASSERT(
      board[Location(1,5)].token() == Token::Ai
      || board[Location(2,5)].token() == Token::Ai
      || board[Location(3,5)].token() == Token::Ai );
    // reset whichever spot was chosen
    board[Location(1,5)].setToken( Token::Blank );
    board[Location(2,5)].setToken( Token::Blank );
    board[Location(3,5)].setToken( Token::Blank );
    board[Location(1,5)].mDirtyFlags.set(Direction::Vertical);
    board[Location(2,5)].mDirtyFlags.set(Direction::Vertical);
    board[Location(3,5)].mDirtyFlags.set(Direction::Vertical);
    calculator::play( userPlayThree, Token::User, board );
    calculator::computerChoose( board );
    CPPUNIT_ASSERT(
      board[Location(1,5)].token() == Token::Ai
      || board[Location(3,5)].token() == Token::Ai );
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("ComputerChooseResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(ComputerChoose::suite());
  bool success = runner.run();

  return success ? 0 : -1;
}
