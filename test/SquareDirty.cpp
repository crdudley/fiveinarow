#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class SquareDirty : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( SquareDirty );
  CPPUNIT_TEST( InitiallyClean );
  CPPUNIT_TEST_SUITE_END();

public:
  SquareDirty()
    : CppUnit::TestCase( "Initially Clean" )
    {}
  void InitiallyClean()
  {
    Square testSquare;
    CPPUNIT_ASSERT( testSquare.mDirtyFlags.none() );

    testSquare.mDirtyFlags.set( Direction::Vertical );
    CPPUNIT_ASSERT( testSquare.mDirtyFlags.any() );
    CPPUNIT_ASSERT( testSquare.mDirtyFlags.test( Direction::Vertical ) );
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("SquareDirtyResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(SquareDirty::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
