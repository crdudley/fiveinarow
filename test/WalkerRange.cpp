#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class WalkerRange : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( WalkerRange );
  CPPUNIT_TEST( Range );
  CPPUNIT_TEST_SUITE_END();

public:
  WalkerRange()
    : CppUnit::TestCase( "Board Walker Range" )
    {}
  
  void Range()
  {
    Location topLeftOrigin = Location(1,2);
    int rangeSize = static_cast< int >(
      walk::boardRange< walk::HorizontalWalker >( topLeftOrigin ).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 7 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 4 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(topLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );

    Location topRightOrigin = Location(1,gDimension-2);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 6 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(topRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 3 );

    Location bottomLeftOrigin = Location(gDimension-1,2);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 7 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 5 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 5 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(bottomLeftOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 3 );

    Location bottomRightOrigin = Location(gDimension-4,gDimension-5);
    rangeSize = static_cast< int >( walk::boardRange<walk::HorizontalWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 9 );
    rangeSize = static_cast< int >( walk::boardRange<walk::VerticalWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownLeftWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
    rangeSize = static_cast< int >( walk::boardRange<walk::DownRightWalker>(bottomRightOrigin).size() );
    CPPUNIT_ASSERT_EQUAL( rangeSize, 8 );
  }

};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("WalkerRangeResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(WalkerRange::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
