#include "Board.h"
#include "BoardWalker.h"
#include "Location.h"

#include <cppunit/ui/text/TestRunner.h>
#include <cppunit/TestCase.h>
#include <cppunit/TestCaller.h>
#include <cppunit/XmlOutputter.h>
#include <cppunit/extensions/HelperMacros.h>

#include <iostream>
#include <fstream>
#include <string>


class SquareToken : public CppUnit::TestCase
{
  CPPUNIT_TEST_SUITE( SquareToken );
  CPPUNIT_TEST( RoundTripToken );
  CPPUNIT_TEST_SUITE_END();

public:
  SquareToken()
    : CppUnit::TestCase( "Round Trip Token" )
    {}
  void RoundTripToken()
  {
    Square testSquare;
    CPPUNIT_ASSERT( testSquare.token() == Token::Blank );

    testSquare.setToken( Token::User );
    CPPUNIT_ASSERT( testSquare.token() == Token::User );

    testSquare.setToken( Token::Ai );
    CPPUNIT_ASSERT( testSquare.token() == Token::Ai );    
  }
};


int main()
{
  CppUnit::TextUi::TestRunner runner;
  std::ofstream fout("SquareTokenResult.xml");
  CppUnit::XmlOutputter* outputter = new CppUnit::XmlOutputter(&runner.result(),fout);
  runner.setOutputter(outputter);
  runner.addTest(SquareToken::suite());
  bool success = runner.run();
  
  return success ? 0 : -1;
}
