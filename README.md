# Five In A Row

FiveInARow is a two player CLI game (Human vs Computer) that is like an oversized version of Tic Tac Toe. Played on a 10x10 board, the players take turns placing tokens on empty spaces as they try to be the first to get five of their own pieces in a row: vertically, horizontally, or diagonally.

## Getting Started

This project can be downloaded from [Bitbucket](https://bitbucket.org/crdudley/fiveinarow)

### Prerequisites

This project is built with CMake (minimum version 2.8). While technically cross-platform, some features are dependent on Linux, such as the code coverage analysis.

It has been built and tested with gcc 7.3.

### Configuration Options

Pass -DRUN_CODE_COVERAGE=ON to cmake to enable the code coverage analysis of the unit tests.

Pass -DGENERATE_PACKAGE=ON to cmake to generate an installer for this program

## Building

```
mkdir build/
cd build/
cmake ..
make -j8
```

Alternatively, on linux you may run the provided coverage.sh script which will build the program, run the tests and generate the unit test coverage.

```
./coverage.sh
```

## Testing

The unit tests for this project rely on the [CppUnit framework](http://cppunit.sourceforge.net/doc/1.8.0/). In order to run the tests, you will have to install this library and ensure that the `CPPUNIT_LIBRARY` cmake variable is properly set in your CMakeCache.txt file.

The tests can then be run with

```
make test
```

## Documentation

This program is documented with doxygen style comments. In order to understand the decision making philosophy of the AI player, the StateCalculator class would be a good place to start.

## License

This code is released under the CC BY 4.0 license. I hope you find it enjoyable and informative.

## Authors

* **Clifton Dudley** [email](mailto:clifton.r.dudley@gmail.com)